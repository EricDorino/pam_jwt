/*
 * Copyright (C) 2018, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <jwt.h>
#include <stdio.h>

#include <security/pam_appl.h>
#include <security/pam_misc.h>

static int token_conv(int nmsgs, const struct pam_message **msgs,
								struct pam_response **resp, void *data)
{
	int i;

	if (!msgs || !resp || !data)
		return PAM_CONV_ERR;

	if (!(*resp = malloc(nmsgs * sizeof(struct pam_response))))
		return PAM_CONV_ERR;

	for (i = 0; i < nmsgs; i++) {
		resp[i]->resp_retcode = 0;
		resp[i]->resp = NULL;
		switch (msgs[i]->msg_style) {
			case PAM_PROMPT_ECHO_ON:
			case PAM_PROMPT_ECHO_OFF:
				if (strcmp(msgs[i]->msg, "Token: ") == 0)
					resp[i]->resp = strdup((const char *)data);
				break;
			case PAM_TEXT_INFO:
			case PAM_ERROR_MSG:
				break;
			default:
				free(*resp);
				return PAM_CONV_ERR;
		}
	}
	return PAM_SUCCESS;
}


static const char *secret = "secret0";
static const char *header = "{\"alg\":\"HS256\",\"typ\":\"JWT\"}";
static const char *claims = 
	"{\"iss\":\"dorino.fr\",\"sub\":\"eric\"}";
static char token[4096];

int main(int argc, char*argv[]) {
    static struct pam_conv pc = { token_conv, NULL };
    pam_handle_t *ph = NULL;
    int r, ret;

		if (!jwt_sign(token, sizeof token, header, claims, secret)) {
        fprintf(stderr, "Signing failure.\n");
				return 0;
		}
		fprintf(stderr, "token : %s\n", token);
		pc.appdata_ptr = &token;

    if ((r = pam_start("capys_jwt", "eric", &pc, &ph)) != PAM_SUCCESS) {
        fprintf(stderr, "Failure starting pam: %s\n", pam_strerror(ph, r));
        return 1;
    }
    if ((r = pam_authenticate(ph, PAM_SILENT)) != PAM_SUCCESS) {
        fprintf(stderr, "Failed to authenticate: %s\n", pam_strerror(ph, r));
        ret = 1;
    } else {
        printf("Authentication successful.\n");
        ret = 0;
    }
    if ((r = pam_end(ph, r)) != PAM_SUCCESS)
        fprintf(stderr, "Failure shutting down pam: %s\n", pam_strerror(ph, r));

    return ret;
}
