/*
 * Copyright (C) 2018  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/file.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define BEARER SECURITYCONFDIR"/jwt.conf"

int get_secret(const char *issuer, const char *jti,
					const char **alg, const char **secret,
					char *buf, size_t size)
{
	FILE *f;
	char *f_issuer, *f_jti;

	if (!(f = fopen(BEARER, "r")))
		return -1;

	while (fgets(buf, size, f)) {
		if (buf[0] == '#')
			continue;
		if (!(f_issuer = strsep(&buf, ":"))) {
			errno = EINVAL;
			goto not_found;
		}
		f_jti = strsep(&buf, ":");
		*alg = strsep(&buf, ":");
		*secret = strsep(&buf, "\n");
		if (!(strcmp(f_issuer, issuer) == 0 && strcmp(f_jti, jti) == 0))
			continue;
		if (!(alg && secret))
			goto not_found;
		errno = 0;
		fclose(f);
		return 0;
	}

	not_found:
	fclose(f);
	return -1;
}

