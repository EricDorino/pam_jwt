/*
 * Copyright (C) 2016,2018  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <json.h>
#include <jwt.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT

#include <security/pam_appl.h>
#include <security/pam_ext.h>
#include <security/pam_modules.h>

extern int
get_secret(const char *issuer, const char *jti,
					 const char **alg, const char **secret, 
					 char *, size_t);

static int get_token(pam_handle_t *pamh)
{
	int ret;
	char *token;

	ret = pam_prompt(pamh, PAM_PROMPT_ECHO_ON, &token, "Token: ");
	if (ret != PAM_SUCCESS)
		return ret;
	if (!token)
		return PAM_CONV_ERR;
	return pam_set_item(pamh, PAM_AUTHTOK, token);
}

static int auth(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	int ret;
	const char *token;
	struct json *h, *c;
	const char *issuer, *sub, *user, *jti, *fake_alg, *typ;
	long expire, not_before;
	struct passwd passwd;

	if ((ret = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS) {
		pam_syslog(pamh, LOG_DEBUG, "pam_get_user(): %s", pam_strerror(pamh, ret));
		return ret;
	}
	if ((ret = get_token(pamh)) != PAM_SUCCESS) {
		pam_syslog(pamh, LOG_DEBUG, "get_token(): %s", pam_strerror(pamh, ret));
		return ret;
	}
	if ((ret = pam_get_item(pamh, PAM_AUTHTOK, (const void **)&token)) !=
									PAM_SUCCESS) {
		pam_syslog(pamh, LOG_DEBUG, "pam_get_item(AUTHTOK): %s", 
										pam_strerror(pamh, ret));
		return ret;
	}
	if (jwt_info(token, &h, &c) < 0) {
		pam_syslog(pamh, LOG_CRIT, "Invalid token");
		ret = PAM_AUTH_ERR;
		goto the_end;
	}
	if (!(json_find_string(c, "iss", &issuer) &&
				json_find_string(c, "sub", &sub) && 
				json_find_string(h, "alg", &fake_alg) && 
				json_find_string(h, "typ", &typ) && 
				strcmp(user, sub) == 0 &&
				strcmp(typ, "JWT") == 0)) {
		pam_syslog(pamh, LOG_CRIT, "Invalid token");
		ret = PAM_AUTH_ERR;
		goto the_end;
	}

	if (!json_find_string(c, "jti", &jti))
		jti = "*";

	char buf[4096];
	const char *alg, *secret;
	if (get_secret(issuer, jti, &alg, &secret, buf, sizeof(buf)) < 0) {
		pam_syslog(pamh, LOG_CRIT, "Invalid token");
		ret = PAM_AUTH_ERR;
		goto the_end;
	}

	if (jwt_verify(token, alg, secret) < 0) {
		pam_syslog(pamh, LOG_CRIT, "Invalid token");
		ret = PAM_AUTH_ERR;
		goto the_end;
	}
	if (!getpwnam(user)) {
		pam_syslog(pamh, LOG_CRIT, "User %s unknown", user);
		ret = PAM_USER_UNKNOWN;
		goto the_end;
	}
	if (json_find_int(c, "exp", &expire) && expire <= time(NULL)) {
		pam_syslog(pamh, LOG_CRIT, "Expired token");
		ret = PAM_AUTH_ERR;
		goto the_end;
	}
	if (json_find_int(c, "nbf", &not_before) && not_before >= time(NULL)) {
		pam_syslog(pamh, LOG_CRIT, "Unusable token");
		return PAM_AUTH_ERR;
	}
	ret = pam_set_item(pamh, PAM_USER, user);
the_end:
	json_free(h);
	json_free(c);
	return ret;
}

PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return auth(pamh, flags, argc, argv);
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return auth(pamh, flags, argc, argv);
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh,
								int flags, 
								int argc, const char **argv)
{
	return PAM_IGNORE;
}

#ifdef PAM_STATIC
struct pam_module _pam_jwt_modstruct = {
	"pam_jwt",
	pam_sm_authenticate,
	pam_sm_setcred,
	pam_sm_acct_mgmt,
	pam_sm_open_session,
	pam_sm_close_session,
	pam_sm_chauthtok
};
#endif
